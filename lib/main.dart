import 'package:flutter/material.dart';
import 'package:state_management/bloc/auth/auth_bloc.dart';
import 'package:state_management/bloc/auth/auth_state.dart';
import 'package:state_management/ui/change_name_screen.dart';
import 'package:state_management/ui/loading_screen.dart';
import 'package:state_management/ui/signin_screen.dart';
import 'package:state_management/ui/main_screen.dart'; 
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_management/ui/signup_screen.dart';

void main () => runApp(AppBase());

class AppBase extends StatelessWidget {
  @override
  Widget build(BuildContext context) => BlocProvider <AuthBloc>(
      builder: (context) => AuthBloc(),
      child: MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocBuilder <AuthBloc, AuthState>(
          builder: (context, state) {
            if(state is Authenticated) return MainScreen(state.name);
            if(state is AuthError || state is Unauthenticated) return SignInScreen();
            if(state is ModifyingError) return ChangeNameScreen();
            if(state is NotExist) return SignUpScreen();
            return LoadingScreen();
          }
        ),
    ),
  );
}