import 'package:flutter/material.dart';
import 'package:state_management/bloc/auth/auth_bloc.dart';
import 'package:state_management/bloc/auth/auth_event.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_management/bloc/auth/auth_state.dart';

class SignInScreen extends StatelessWidget {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) => Scaffold(
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextField(
            decoration: InputDecoration(
              hintText: 'username'
            ),
            controller: usernameController
          ),
          TextField(
            decoration: InputDecoration(
              hintText: 'password'
            ),
            controller: passwordController
          ),
          BlocBuilder<AuthBloc, AuthState>(
            builder: (context, state){
              if(state is UsernameError) return Text('Username Error');
              if(state is PasswordError) return Text('Password Error');
              return Text('');
            }
          ),
          RaisedButton(
            child: Text(
              'Log In',
            )
            ,onPressed: (){
              BlocProvider.of<AuthBloc>(context).add(
                SignIn(usernameController.text, passwordController.text)
              );
            },
          )
        ]
      )
    )
  );
}