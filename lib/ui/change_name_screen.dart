import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_management/bloc/auth/auth_event.dart';
import 'package:state_management/bloc/auth/auth_bloc.dart';
import 'package:state_management/bloc/auth/auth_state.dart';
import 'package:state_management/ui/main_screen.dart';

class ChangeNameScreen extends StatelessWidget {
  final nameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) => Scaffold(
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextField(
            decoration: InputDecoration(
              hintText: 'name'
            ),
            controller: nameController,
          ),
          TextField(
            decoration: InputDecoration(
              hintText: 'password'
            ),
            controller: passwordController,
          ),
          BlocBuilder<AuthBloc,AuthState>(
            builder: (context, state){
              if(state is VerifyPasswordError) return Text('Wrong Password');
              if(state is ChangeNameError) return Text('Something Went Wrong');
              return Text('');
            }
          ),
          SizedBox(
            height: 100,
            width: 100,
            child: RaisedButton(
              child: Text('Change Name'),
              onPressed: () {
                BlocProvider.of<AuthBloc>(context).add(
                  ChangeName(nameController.text, passwordController.text)
                );
              },
            ),
          )
        ],
      )
    )
  );
}