import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_management/bloc/auth/auth_bloc.dart';
import 'package:state_management/bloc/auth/auth_event.dart';
import 'package:state_management/bloc/auth/auth_state.dart';

class SignUpScreen extends StatelessWidget {
  final nameController = TextEditingController();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) => Scaffold(
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextField(
            decoration: InputDecoration(
              hintText: 'Full Name'
            ),
            controller: nameController
          ),
          TextField(
            decoration: InputDecoration(
              hintText: 'Username'
            ),
            controller: usernameController
          ),
          TextField(
            decoration: InputDecoration(
              hintText: 'Password'
            ),
            controller: passwordController
          ),
          BlocBuilder<AuthBloc, AuthState>(
            builder: (context, state){
              if(state is SignUpError) return Text('Something Wrong');
              return Text('');
            }
          ),
          RaisedButton(
            child: Text('Sign Up'),
            onPressed: (){
              BlocProvider.of<AuthBloc>(context).add(
                SignUp(nameController.text, usernameController.text, passwordController.text)
              );
            },
          )
        ]
      )
    )
  );
}