import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:state_management/bloc/auth/auth_event.dart';
import 'package:state_management/bloc/auth/auth_bloc.dart';

import 'change_name_screen.dart';

class MainScreen extends StatelessWidget {
  final name;
  MainScreen(this.name);
  @override
  Widget build(BuildContext context) => Scaffold(
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RaisedButton(
            child: Text('Edit Name'),
            onPressed: (){
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => ChangeNameScreen()
                )
              );
            }
          ),
          RaisedButton(
            child: Text('Sign Out'),
            onPressed: (){
              BlocProvider.of<AuthBloc>(context).add(
                SignOut()
              );
            }
          ),
          Text(
            this.name
          )
        ]
      )
    )
  );
}
