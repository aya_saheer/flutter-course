abstract class AuthEvent {}

class AppStarted extends AuthEvent{}

class SignUp extends AuthEvent{
  final String name, username, password;

  SignUp(this.name, this.username, this.password);
}

class SignOut extends AuthEvent{}

class SignIn extends AuthEvent{
  final String username, password;

  SignIn(this.username, this.password);
}

class ChangeName extends AuthEvent{
  final name, password;

  ChangeName(this.name, this.password);
}