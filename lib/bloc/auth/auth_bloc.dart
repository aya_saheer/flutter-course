import 'package:state_management/bloc/auth/auth_exception.dart';

import 'auth_event.dart';
import 'auth_state.dart';
import 'auth_util.dart';
import 'package:bloc/bloc.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState>{
  final util = AuthUtil();

  AuthBloc() {
    this.add(AppStarted());
  }

  @override
  AuthState get initialState => Loading();

  @override
  void onError(Object error, StackTrace stacktrace) {
    super.onError(error, stacktrace);
    print(error.toString() + ":" + stacktrace.toString());
  }

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async*{
    if(event is AppStarted){
      yield Loading();
      final exist = await util.isExist();
      final logged = await util.isLoggedIn();
      yield (exist)? ((logged)? Authenticated(await util.getName()) : Unauthenticated()) : NotExist();
    }

    if(event is SignIn){
      yield Loading();
      try{
        final name = await util.signIn(event.username, event.password);
        yield Authenticated(name);
      }
      catch(error){
        yield (error is PasswordException)? PasswordError() : UsernameError();
      }
    }

    if(event is SignUp){
      final success = await util.signUp(event.name, event.username, event.password);
      yield (success)? Authenticated(await util.getName()) : SignUpError();
    }

    if(event is SignOut){
      final success = await util.signOut();
      yield (success)? Unauthenticated() : SignOutError();
    }

    if(event is ChangeName){
      yield Loading();
      try{
        final success = await util.changeName(event.name, event.password);
        yield (success)? Authenticated(await util.getName()) : ChangeNameError();
      }
      catch(error){
        yield VerifyPasswordError();
      } 
    }
  }

}