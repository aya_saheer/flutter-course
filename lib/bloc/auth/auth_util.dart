import 'package:shared_preferences/shared_preferences.dart';
import 'package:state_management/bloc/auth/auth_exception.dart';

const USERNAME_KEY = 'username';
const PASSWORD_KEY = 'password';
const NAME_KEY = 'name';
const LOGGED_IN_KEY = 'logged_in';

class AuthUtil{
  final instance = SharedPreferences.getInstance();

  Future<bool> isExist() async{
    final instance = await this.instance;
    return instance.containsKey(USERNAME_KEY);
  }

  Future<bool> isLoggedIn() async{
    final instance = await this.instance;
    return instance.getBool(LOGGED_IN_KEY);
  }

  Future<String> getName() async{
    final instance = await this.instance;
    return instance.getString(NAME_KEY);
  }

  Future<String> signIn(username, password) async{
    final instance = await this.instance;
    final exist = instance.containsKey(USERNAME_KEY);
    final _username = instance.getString(USERNAME_KEY);
    if((_username != username)||(!exist)) throw UsernameException();
    final _password = instance.getString(PASSWORD_KEY);
    if(_password != password) throw PasswordException();
    instance.setBool(LOGGED_IN_KEY, true);
    return instance.getString(NAME_KEY);
  }

  Future<bool> signUp(name, username, password) async{
    final instance = await this.instance;
    final _name = await instance.setString(NAME_KEY, name);
    final _username = await instance.setString(USERNAME_KEY, username);
    final _password = await instance.setString(PASSWORD_KEY, password);
    instance.setBool(LOGGED_IN_KEY, true);
    return _name && _username && _password;
  }

  Future<bool> signOut() async{
    final instance = await this.instance;
    return instance.setBool(LOGGED_IN_KEY, false);
  }

  Future<bool> changeName(name, password) async{
    final instance = await this.instance;
    final _password = instance.getString(PASSWORD_KEY);
    if(password != _password) throw PasswordException();
    return instance.setString(NAME_KEY, name);
  }
}