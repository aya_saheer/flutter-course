abstract class AuthState{}

abstract class AuthError extends AuthState{}

abstract class ModifyingError extends AuthState{}

class Loading extends AuthState{}

class Authenticated extends AuthState{
  final String name;
  Authenticated(this.name);
} 

class Unauthenticated extends AuthState{}

class NotExist extends AuthState{}

class UsernameError extends AuthError{}

class PasswordError extends AuthError{}

class SignUpError extends AuthError{}

class SignOutError extends AuthError{}

class ChangeNameError extends ModifyingError{}

class VerifyPasswordError extends ModifyingError{}