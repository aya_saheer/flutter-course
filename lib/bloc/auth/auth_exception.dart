abstract class AuthException{}

class UsernameException extends AuthException{}

class PasswordException extends AuthException{}